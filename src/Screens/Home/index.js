import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getPokemon, setFavoritPokemon } from "Store/Action/Pokemon";

import Layout from "Components/Layout";

class Home extends Component {
  constructor() {
    super();
    this.state = {
      limit: 10,
      offset: 2,
    };
  }

  componentDidMount() {
    this.props.getPokemon({
      lim: this.state.limit,
      offs: this.state.offset,
    });
  }

  render() {
    const { offset } = this.state;
    const { pokemons, favorits } = this.props;

    return (
      <Layout>
        <div className="home">
          <div className="home__title">Poke Fav</div>
          <div className="home__grid container">
            {favorits.length === 0
              ? null
              : favorits.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <Link
                        className="home__grid__item__content"
                        to={`/pokemon/${offset + index + 1}`}
                      >
                        <span>{pokemon?.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>

          <div className="home__title">Poke List</div>
          <div className="home__grid container">
            {pokemons.length === 0
              ? null
              : pokemons.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <button
                        className="home__grid__item__save"
                        onClick={() =>
                          this.props.setFavoritPokemon({
                            id: offset + index + 1,
                            name: pokemon.name,
                          })
                        }
                      >
                        +
                      </button>

                      <Link
                        className="home__grid__item__content"
                        to={`/pokemon/${offset + index + 1}`}
                      >
                        <span>{pokemon.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>
        </div>
      </Layout>
    );
  }
}

//map state to props adalag fungsi untuk me-mapping data dari store-redux ke komponen yang menggunakan (terbaca sebagai props)
const mapStateToProps = (state) => {
  return {
    favorits: state.Pokemon.favoritPokemon,
    pokemons: state.Pokemon.pokemons,
  };
};

//mapping dispatch as a props
// const mapDispatchToProps = (dispatch) => {
//   return {
//     handleAddFavorit: (params) =>
//       dispatch({ type: "SET_FAVORIT_POKEMON", newValue: params.newValue }),
//   };
// };

//export default connect(mapping-to-props)(component-name)
export default connect(mapStateToProps, { getPokemon, setFavoritPokemon })(
  Home
);
