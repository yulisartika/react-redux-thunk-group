import axios from "axios";

export const getPokemon = (parameter) => (dispatch) => {
  const { lim, offs } = parameter;
  axios
    .get(`https://pokeapi.co/api/v2/pokemon?limit=${lim}&offset=${offs}`)
    .then((response) => {
      if (response.status === 200) {
        dispatch({
          type: "GET_POKEMON",
          payload: response.data.results,
        });
      }
    })
    .catch((err) => console.log(err));
};

export const setFavoritPokemon = (params) => (dispatch) => {
  dispatch({
    type: "SET_FAVORIT_POKEMON",
    payload: params,
  });
};
